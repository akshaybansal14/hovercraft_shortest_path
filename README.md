Control of a four-thruster hovercraft:

Consider a four-thruster hovercraft that floats on a air cushion caused by a continuous air flow through a perforated sheet under- neath it. This causes the hovercraft to glide on an almost frictionless surface. The four thrusters are positioned equidistantly from the central axis of the craft and can only push in the forward direction; each thruster can exert a force of at most 2.5 Newton.

The dynamics of the system are given by the linear recurrence
q(k + 1) = A q(k) + B u(k),
where q = (x,y,θ,x ̇,y ̇,θ ̇), u = (u1,u2,u3,u4), and the matrices A and B are given. Note that q(k) is the value of the state q at time kτ, where τ = 0.1 sec is the sampling period. In addition, the control input u is constant over the sampling intervals [kτ,(k + 1)τ) for k = 0,1,2,.... The objective here is to design a trajectory from stationary point q(0) = (0, 0, 0, 0, 0, 0) to stationary point q(N ) = (0, 10, π/2, 0, 0, 0), for some positive integer N

We would like to find the control input u(0), u(1), . . . , u(N − 1), for some integer N , that would bring the hovercraft to the stationary point q(N ) = (0, 10, π/2, 0, 0, 0) as quickly as possible.