import cvxpy as cp
import numpy as np
import sys
import math

class hovercraft:

    # Constructor to initialize the input values
    def __init__(self, **kwargs):
        self.A = kwargs['A']
        self.B = kwargs['B']
        self.F_min = kwargs['F_min']
        self.F_max = kwargs['F_max']
        self.q_init = kwargs['q_init']
        self.q_dest = kwargs['q_dest']
        self.T_max = kwargs['T_max']
    
    # Method to extract dimensions of the given input matrix
    def __extract_dim(self):
        if (self.A.shape[0] == self.A.shape[1]):
            self.len_q = self.A.shape[0]
        else:
            print('Incorrect dimensions for matrix A')
            sys.exit()
        if (self.B.shape[0] == self.len_q):
            self.len_u = self.B.shape[1]
        else:
            print('Incorrect dimensions for matrix B')
            sys.exit()

    # Create variables for optimizating path of the hovercraft
    def __create_opt_var(self, T):
        q = cp.Variable((self.len_q, T+1))
        u = cp.Variable((self.len_u, T))
        return q, u

    # Add constraints for the optimization problem
    def __add_constraints(self, q, u, T):
        # Additional constants and tolerances
        intersect_point = 4.5
        ep = 0.0001
        ep_vec = np.full(self.len_q, ep)

        # Populating constraints
        constr = []
        F_ub = np.full(self.len_u, self.F_max)
        F_lb = np.full(self.len_u, self.F_min)
        for t in range(T):
            constr += [q[:,t+1] == self.A@q[:,t] + self.B@u[:,t], u[:,t] >= F_lb, u[:,t] <= F_ub] # dynamics of the movement
        constr += [q[:,0] == self.q_init] # start location of the hovercraft
        constr += [q[:,T] >= self.q_dest - ep_vec, q[:,T] <= self.q_dest + ep_vec] # final location of the hovercraft

        # Adding bigM constraints to handle the obstacle (rectangular)
        M = 100; # sufficiently large bigM value
        rect_right = 3; rect_bot = 3; rect_top = 6;
        y_mid = cp.Variable(T, boolean = True)
        y_bot = cp.Variable(T, boolean = True)
        y_top = cp.Variable(T, boolean = True)
        for t in range(T):   
            constr += [q[0][t] <= rect_right + M * (1 - y_mid[t]), q[0][t] >= rect_right - M * y_mid[t]]
            constr += [q[1][t] <= rect_bot + M * (1 - y_bot[t]), q[1][t] >= rect_bot - M * y_bot[t]]
            constr += [q[1][t] >= rect_top - M * (1 - y_top[t]), q[1][t] <= rect_top + M * y_top[t]]
            constr += [y_bot[t] + y_top[t] <= 1 + M * (1 - y_mid[t]), y_bot[t] + y_top[t] >= 1 - M * (1 - y_mid[t])]

        return constr

    # Optimize for the best time using bisection method 
    def optimize_bisection(self):
        self.__extract_dim()
        lb = 0; ub = self.T_max
        while(1):
            T = (int)(math.ceil((lb + ub) / 2))
            q, u = self.__create_opt_var(T)
            prob = cp.Problem(cp.Minimize(0), self.__add_constraints(q, u, T))
            result = prob.solve(solver = cp.GLPK_MI)
            if (prob.status == 'infeasible'):
                lb = T
            elif (prob.status == 'optimal'):
                ub = T
            if (ub <= lb + 1 and prob.status == 'optimal'):
                break

        return q, u, T