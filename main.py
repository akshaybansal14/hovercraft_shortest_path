import matplotlib.pyplot as plt
import numpy as np
from hovercraft import hovercraft

def plot_trajectories(q, u, T):

    grid_shape = (2,3)
    y_labels = [['x', 'y', 'theta'], ['velocity_x', 'velocity_y', 'angular_velocity']]
    fig, ax = plt.subplots(grid_shape[0], grid_shape[1])
    for x in range(grid_shape[0]):
        for y in range(grid_shape[1]):
            ax[x,y].plot(range(T + 1), q.value[x * grid_shape[1] + y,:])
            ax[x,y].set(ylabel = y_labels[x][y], xlabel = 'time')
    #fig.savefig('q_t.png')
    
    grid_shape = (2,2)
    y_labels = [['u1', 'u2'], ['u3', 'u4']]
    fig, ax = plt.subplots(grid_shape[0], grid_shape[1])
    for x in range(grid_shape[0]):
        for y in range(grid_shape[1]):
            ax[x,y].plot(range(T), u.value[x * grid_shape[1] + y,:])
            ax[x,y].set(ylabel = y_labels[x][y], xlabel = 'time')
    #fig.savefig('u_t.png')

    fig, ax = plt.subplots()
    ax.plot(q.value[0,:], q.value[1,:])
    ax.set(xlabel = 'x-coordinate', ylabel = 'y-coordinate')
    #fig.savefig('x_y.png')

    return

# Input Data
A_mat = np.array([[1.0000,     0,        0,     0.1000,         0,         0],
    [0,    1.0000,        0,          0,    0.1000,         0],
    [0,         0,    1.0000,         0,         0,    0.0999],
    [0,         0,         0,    0.9998,         0,         0],
    [0,         0,         0,         0,    0.9998,         0],
    [0,         0,         0,         0,         0,    0.9985]])


B_mat = np.array([[0.0020,    0.0020,   -0.0020,   -0.0020],
    [0.0020,   -0.0020,   -0.0020,    0.0020],
    [0.0318,   -0.0318,    0.0318,   -0.0318],
    [0.0409,    0.0409,   -0.0409,   -0.0409],
    [0.0409,   -0.0409,   -0.0409,    0.0409],
    [0.6351,   -0.6351,    0.6351,   -0.6351]]);

F_max = 2.5
F_min = 0
q_init_vec = np.array([0, 0, 0, 0, 0, 0])
q_dest_vec = np.array([0, 10, np.pi/2.0, 0, 0, 0])

instance = hovercraft(A = A_mat, B = B_mat, F_min = 0, F_max = 2.5, q_init = q_init_vec, q_dest = q_dest_vec, T_max = 100)


q, u, T = instance.optimize_bisection()
plot_trajectories(q, u, T)